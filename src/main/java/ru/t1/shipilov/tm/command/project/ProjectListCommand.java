package ru.t1.shipilov.tm.command.project;

import ru.t1.shipilov.tm.enumerated.Sort;
import ru.t1.shipilov.tm.enumerated.Status;
import ru.t1.shipilov.tm.model.Project;
import ru.t1.shipilov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    private final String NAME = "project-list";

    private final String DESCRIPTION = "Show project list.";

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[SHOW PROJECTS]");
        System.out.println("ENTER SORT");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = getProjectService().findAll(userId, sort);
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName() + ": " + project.getDescription() + ", "
                    + Status.toName(project.getStatus()));
            index++;
        }
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
