package ru.t1.shipilov.tm.command.project;

import ru.t1.shipilov.tm.model.Project;
import ru.t1.shipilov.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    private final String NAME = "project-remove-by-id";

    private final String DESCRIPTION = "Remove project by Id.";

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(userId, id);
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
