package ru.t1.shipilov.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    private final String NAME = "version";

    private final String DESCRIPTION = "Show version info.";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.20.0");
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
