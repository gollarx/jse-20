package ru.t1.shipilov.tm.command.user;

import ru.t1.shipilov.tm.enumerated.Role;
import ru.t1.shipilov.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    private final String NAME = "update-user-profile";

    private final String DESCRIPTION = "Update profile of current user.";

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("FIRST NAME");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME");
        final String middleName = TerminalUtil.nextLine();
        getUserService().updateUser(
                userId, firstName, lastName, middleName
        );
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
